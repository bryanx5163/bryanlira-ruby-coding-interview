# frozen_string_literal: true

class TweetsController < ApplicationController
  def index
    render json: Tweet.all
  end
end
