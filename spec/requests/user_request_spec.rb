# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Users", type: :request do
  RSpec.shared_context("with multiple companies") do
    let!(:company1) { create(:company) }
    let!(:company2) { create(:company) }

    before do
      5.times do
        create(:user, company: company1)
      end
      5.times do
        create(:user, company: company2)
      end
    end
  end

  describe "#index" do
    let(:result) { JSON.parse(response.body) }

    context "when fetching users by company" do
      include_context "with multiple companies"

      it "returns only the users for the specified company" do
        get company_users_path(company1)

        expect(result.size).to eq(company1.users.size)
        expect(result.map { |element| element["id"] }).to eq(company1.users.ids)
      end
    end

    context "when fetching all users" do
      include_context "with multiple companies"

      it "returns all the users" do
      end
    end
  end
end
